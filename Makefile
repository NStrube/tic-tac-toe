.POSIX:

CC=cc
CFLAGS=-Wall -Wextra -std=c99 -pedantic
CFLAGS+=-fsanitize=address -fsanitize=undefined -ggdb
CFLAGS+=-lSDL2

ttt: main.c
	$(CC) $(CFLAGS) -o $@ main.c

clean:
	rm -f ttt

.PHONY: clean
