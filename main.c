#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <SDL2/SDL.h>

#define _STATE_H_IMPLEMENTATION
#define _SDL_H_IMPLEMENTATION
#include "sdl.h"

int validate_input(char* in, int* n) {
    char* end = NULL;
    int t = (int) strtol(in, &end, 10);

    if (in == end || *end != '\0') {
        printf("Ptrs\n");
        return Error_InvalidInput;
    }

    if (t < 1 || t > 9) return Error_InvalidInput;

    *n = t;

    return Error_Ok;
}

int check_win(void) {
    for (int i = 0; i < 3; i++) {
        int j = i*3;
        // Horizontal.
        if (board[j] != EMPTY && board[j] == board[j+1] && board[j] == board[j+2]) {
            return board[j];
        }
        // Vertical.
        if (board[i] != EMPTY && board[i] == board[i+3] && board[i] == board[i+6]) {
            return board[i];
        }
    }

    // Diagonal
    if (board[4] != EMPTY
        && ((board[0] == board[4] && board[8] == board[4])
            || (board[2] == board[4] && board[6] == board[4]))) {
        return board[4];
    }

    return EMPTY;
}

int term_main(void) {
    board_reset();

    char curr_player = PLAYER_O;

    char* line = NULL;
    size_t cap = 0;
    ssize_t len = 0;

    board_print();

    while ((len = getline(&line, &cap, stdin)) > 0) {
        line[len-1] = '\0';

        if (strcmp(line, "exit") == 0) break;

        int n = 0;
        Error e = validate_input(line, &n);
        if (e != Error_Ok) {
            printf("Invalid input.\n");
            continue;
        }

        e = board_set(curr_player, n);
        if (e != Error_Ok) {
            printf("Invalid square.\n");
            continue;
        }

        board_print();

        int win = check_win();
        if (win != EMPTY) {
            printf("Player %c won!\n", win);
            break;
        }

        curr_player = curr_player == PLAYER_O ? PLAYER_X : PLAYER_O;
    }

    free(line);
    return 0;
}

int sdl_main(void) {
    SDL_Window* w = NULL;
    SDL_Renderer* r = NULL;

    if (sdl_init(&w, &r) < 0) {
        printf("Failed to initialize SDL2: %s\n", SDL_GetError());
        return 1;
    }

    board_reset();

    int should_quit = 0;
    char curr_player = PLAYER_O;
    SDL_Event e;
    Error err = Error_Ok;

    while (!should_quit) {
        int switch_player = 0;
        // TODO: Disable input on win
        while (SDL_PollEvent(&e)) {
            should_quit |= sdl_handle_event(curr_player, &switch_player, &err, &e);
        }

        int win = check_win();
        if (win != EMPTY) {
            // TODO: Draw win instead, let user quit window.
            printf("Player %c won!\n", win);
            break;
        }

        if (switch_player)
            curr_player = curr_player == PLAYER_O
                ? PLAYER_X
                : PLAYER_O;

        sdl_draw_board(r);
    }

    sdl_quit(&w, &r);
    return 0;
}

int main(void) {
    return sdl_main();
}
