use sdl2::{
    event::Event,
    keyboard::Keycode,
    pixels::Color,
    rect::{Point, Rect},
    render::{Canvas, RenderTarget},
};

fn main() {
    let context = sdl2::init().unwrap();
    let video = context.video().unwrap();
    let window = video
        .window("Tic Tac Toe", 800, 600)
        .position_centered()
        .build()
        .unwrap();

    let mut canvas = window.into_canvas().build().unwrap();

    canvas.set_draw_color(Color::RGB(0, 0, 255));
    canvas.clear();
    canvas.present();

    let mut state = State::default();
    let mut event_pump = context.event_pump().unwrap();

    while !state.should_quit {
        canvas.set_draw_color(Color::RGB(0, 0, 255));
        canvas.clear();

        event_pump
            .poll_iter()
            .try_for_each(|e| state.handle_event(e))
            .unwrap();

        if let Some(winner) = state.check_win() {
            // TODO: Write winner on screen, allow for exiting or restarting
            println!("{:?} won!", winner);
            state.should_quit = true;
        }

        state.draw(&mut canvas).unwrap();

        canvas.present();
    }
}

#[derive(Default, Debug, Clone, Copy, PartialEq, Eq)]
enum Player {
    #[default]
    O,
    X,
}

impl Player {
    fn draw<T: RenderTarget>(&self, canvas: &mut Canvas<T>, idx: usize, width: u32, height: u32) -> Result<(), String> {
        if idx > 9 {
            unreachable!("Can only be called with idx between 1-9");
        }

        let x = idx as i32 % 3;
        let y = idx as i32 / 3;

        let w = width as i32;
        let h = height as i32;

        match self {
            Self::O => {
                // TODO: Draw circle
                let rect = Rect::new(w*x+w/4, h*y+h/4, width/2, height/2);
                canvas.fill_rect(rect)?;
            },
            Self::X => {
                let x1 = w*x+w/4;
                let x2 = w*x+w*3/4;
                let y1 = h*y+h/4;
                let y2 = h*y+h*3/4;

                // TODO: Draw thicker.
                canvas.draw_line(Point::new(x1, y1), Point::new(x2, y2))?;
                canvas.draw_line(Point::new(x1, y2), Point::new(x2, y1))?;
            },
        }

        Ok(())
    }
}

#[derive(Default, Debug)]
struct State {
    board: [Option<Player>; 9],
    curr_player: Player,
    should_quit: bool,
}

impl State {
    fn draw<T: RenderTarget>(&self, canvas: &mut Canvas<T>) -> Result<(), String> {
        let (width, height) = canvas.output_size()?;

        canvas.set_draw_color(Color::RGB(200, 100, 0));

        let width_3 = width / 3;
        let height_3 = height / 3;

        {
            let width = width as i32;
            let height = height as i32;
            let width_3 = width_3 as i32;
            let height_3 = height_3 as i32;

            // TODO: Draw thicker.
            canvas.draw_line(Point::new(width_3, 0), Point::new(width_3, height))?;
            canvas.draw_line(Point::new(width_3*2, 0), Point::new(width_3*2, height))?;
            canvas.draw_line(Point::new(0, height_3), Point::new(width, height_3))?;
            canvas.draw_line(Point::new(0, height_3*2), Point::new(width, height_3*2))?;
        }

        for (i, field) in self.board.iter().enumerate() {
            match field {
                Some(field) => field.draw(canvas, i, width_3, height_3)?,
                None => {},
            }
        }

        Ok(())
    }

    fn handle_event(&mut self, event: Event) -> Result<(), String> {
        match event {
            Event::Quit { .. }
            | Event::KeyDown {
                keycode: Some(Keycode::Escape),
                ..
            } => self.should_quit = true,
            Event::KeyDown { keycode: Some(keycode), .. } if keycode_contains(Keycode::Num1..=Keycode::Num9, keycode) => {
                let n = keycode as i32 - Keycode::Num1 as i32;
                self.set(n as usize)?;
            },
            _ => {}
        }

        Ok(())
    }

    fn set(&mut self, idx: usize) -> Result<(), String> {
        if idx > 9 {
            unreachable!("Can only be called with idx between 1-9");
        }

        if self.board[idx].is_some() {
            return Err("Already occupied square")?;
        }

        self.board[idx] = Some(self.curr_player);
        self.next_player();

        Ok(())
    }

    fn next_player(&mut self) {
        self.curr_player = if matches!(self.curr_player, Player::O) {
            Player::X
        } else {
            Player::O
        };
    }

    fn check_win(&self) -> Option<Player> {
        let board = &self.board;
        for i in 0..3 {
            let j = i*3;
            // Horizontal
            if board[j].is_some() && board[j] == board[j+1] && board[j] == board[j+2] {
                return board[j];
            }
            // Vertical
            if board[i].is_some() && board[i] == board[i+1] && board[i] == board[i+2] {
                return board[i];
            }
        }

        if board[4].is_some() && (board[0] == board[4] && board[4] == board[8]
            || board[4] == board[2] && board[4] == board[6]) {
            return board[4];
        }

        None
    }
}

fn keycode_contains(range: std::ops::RangeInclusive<Keycode>, key: Keycode) -> bool {
    let start = *range.start() as i32;
    let end = *range.end() as i32;
    let key = key as i32;
    (start..=end).contains(&key)
}
