#include <assert.h>
#include <SDL2/SDL.h>

#include "state.h"

#ifndef _SDL_H
#define _SDL_H

int sdl_init(SDL_Window**, SDL_Renderer**);
void sdl_quit(SDL_Window**, SDL_Renderer**);
int sdl_handle_event(int, int*, Error*, const SDL_Event*);
void sdl_draw_board(SDL_Renderer*);

#endif // _SDL_H

#ifdef _SDL_H_IMPLEMENTATION

int sdl_init(SDL_Window** w, SDL_Renderer** r) {
    if (!w || !r) return -1;

    int res = SDL_Init(SDL_INIT_VIDEO);
    if (res < 0) goto defer;

    *w = SDL_CreateWindow(
            "Tic Tac Toe",
            SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,
            SCREEN_WIDTH,
            SCREEN_HEIGHT,
            SDL_WINDOW_SHOWN);
    if (!*w) goto defer;

    *r = SDL_CreateRenderer(*w, -1, 0);
    if (!*r) goto defer;

    return 0;

defer:
    sdl_quit(w, r);
    return -1;
}

void sdl_quit(SDL_Window** w, SDL_Renderer** r) {
    if (r && *r) SDL_DestroyRenderer(*r);
    if (w && *w) SDL_DestroyWindow(*w);
    SDL_Quit();
}

int sdl_handle_event(int curr_player, int* switch_player, Error* err, const SDL_Event* e) {
    assert(switch_player && err && e);

    switch (e->type) {
        case SDL_QUIT:
            return 1;
        case SDL_KEYDOWN:
            switch (e->key.keysym.sym) {
                case SDLK_ESCAPE:
                case SDLK_q:
                    return 1;
                case SDLK_1:
                case SDLK_2:
                case SDLK_3:
                case SDLK_4:
                case SDLK_5:
                case SDLK_6:
                case SDLK_7:
                case SDLK_8:
                case SDLK_9:
                    *err = board_set(curr_player, e->key.keysym.sym - SDLK_0);
                    *switch_player = *err == Error_Ok;
                    break;
            }
            break;
        default: break;
    }

    return 0;
}

static void sdl_draw_x(SDL_Renderer* r, int x, int y) {
    assert(0 < x && x <= 3 && "X must be inside the board");
    assert(0 < y && y <= 3 && "Y must be inside the board");

    int left = cell_width*(x-1) + cell_width / 4;
    int right = cell_width*x - cell_width / 4;
    int top = cell_height*(y-1) + cell_height / 4;
    int bottom = cell_height*y - cell_height / 4;

    SDL_SetRenderDrawColor(r, 255, 0, 0, 255);
    SDL_RenderDrawLine(r, left, top, right, bottom);
    SDL_RenderDrawLine(r, right, top, left, bottom);
}

static void sdl_draw_o(SDL_Renderer* r, int x, int y) {
    assert(0 < x && x <= 3 && "X must be inside the board");
    assert(0 < y && y <= 3 && "Y must be inside the board");

    x--; y--;

    // TODO: Draw circle
    SDL_SetRenderDrawColor(r, 255, 0, 0, 255);
    SDL_Rect rect = {
        .x = cell_width/4+cell_width*x,
        .y = cell_height/4+cell_height*y,
        .w = cell_width/2,
        .h = cell_height/2,
    };
    SDL_RenderDrawRect(r, &rect);
}

// TODO: Accept colors
// TODO: Show message if error occured
void sdl_draw_board(SDL_Renderer* r) {
    SDL_SetRenderDrawColor(r, 23, 23, 23, 255);
    SDL_RenderClear(r);

    SDL_SetRenderDrawColor(r, 255, 255, 255, 255);
    SDL_RenderDrawLine(r, cell_width, 0, cell_width, SCREEN_HEIGHT);
    SDL_RenderDrawLine(r, cell_width*2, 0, cell_width*2, SCREEN_HEIGHT);
    SDL_RenderDrawLine(r, 0, cell_height, SCREEN_WIDTH, cell_height);
    SDL_RenderDrawLine(r, 0, cell_height*2, SCREEN_WIDTH, cell_height*2);

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            switch (board[i+j*3]) {
                case PLAYER_O:
                    sdl_draw_o(r, i+1, j+1);
                    break;
                case PLAYER_X:
                    sdl_draw_x(r, i+1, j+1);
                    break;
                case EMPTY:
                    break;
                default: assert(0 && "unreachable");
            }
        }
    }

    // TODO: Draw win.

    SDL_RenderPresent(r);
}

#endif // _SDL_H_IMPLEMENTATION
