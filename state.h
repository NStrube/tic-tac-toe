#ifndef _STATE_H
#define _STATE_H

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600

typedef enum {
    Error_Ok,
    Error_OutOfBounds,
    Error_InvalidPlayer,
    Error_Occupied,
    Error_InvalidInput
} Error;

enum {
    EMPTY = ' ',
    PLAYER_O = 'O',
    PLAYER_X = 'X',
};

static const int cell_width = SCREEN_WIDTH / 3;
static const int cell_height = SCREEN_HEIGHT / 3;

static char board[9];

void board_reset(void);
Error board_set(const char player, const int field);
void board_print(void);

#endif // _STATE_H

#ifdef _STATE_H_IMPLEMENTATION

void board_reset(void) {
    for (int i = 0; i < 9; i++) board[i] = EMPTY;
}

Error board_set(const char player, int field) {
    if (field <= 0 || field > 9) {
        return Error_OutOfBounds;
    }

    if (player != PLAYER_O && player != PLAYER_X) {
        return Error_InvalidPlayer;
    }

    field -= 1;
    if (board[field] != EMPTY) {
        return Error_Occupied;
    }

    board[field] = player;

    return Error_Ok;
}

void board_print(void) {
    for (int i = 0; i < 9; i+=3) {
        printf(" %c | %c | %c \n", board[i], board[i+1], board[i+2]);
        if (i < 6) printf("---+---+---\n");
    }
}

#endif // _STATE_H_IMPLEMENTATION
